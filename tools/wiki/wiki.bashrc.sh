# file: wiki.bashrc.sh
# wiki.py parameter-completion
# Author: Saeedtx@gmail.com
# To activate just add to bash rc
# . toolkit/tools/wiki/wiki.bashrc.sh

_WikiGetOpt-2 ()   #  By convention, the function name
{                 #+ starts with an underscore.
  local cur
  # Pointer to current completion word.
  # By convention, it's named "cur" but this isn't strictly necessary.

  CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  WIKI_DB="${WIKI_DB:-$CUR_DIR/db}"

  COMPREPLY=()   # Array variable storing the possible completions.

  cur=${COMP_WORDS[COMP_CWORD]}

  WIKI_DIR=""
  if [ -d $cur ]; then
	WIKI_DIR=$cur
  fi
  WIKI_LIST="`ls -1 $WIKI_DB/$WIK_IDIR`"
  case "$cur" in
    -) COMPREPLY=( $( compgen -W "-h -a -e -g -l" -- $cur ) );;
    *)
    COMPREPLY=( $( compgen -W "$WIKI_LIST" -- $cur ) );;
#   Generate the completion matches and load them into $COMPREPLY array.
#   xx) May add more cases here.
#   yy)
#   zz)
  esac

  return 0
}

CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
alias wiki=$CUR_DIR/wiki.py
complete -F _WikiGetOpt-2 -o filenames  wiki tools/wiki/wiki.py $CUR_DIR/wiki.py

