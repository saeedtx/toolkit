#!/usr/bin/python
# Author: Gal Pressman <galpressman@gmail.com>

import argparse
import os
import pydoc
import re
import subprocess

try:
	wiki_db_env = os.environ['WIKI_DB']
except:
	wiki_db_env = ""
script_dir = os.path.dirname(os.path.realpath(__file__))

if not wiki_db_env:
	db_dir = os.path.join(script_dir, 'db')
else:
	db_dir = wiki_db_env

file_path = os.path.join(db_dir, '{file}')
UNDERLINE = '\033[4m'
BLUE = '\033[94m'
UNDERLINED_BLUE = UNDERLINE + BLUE
VANILLA = '\033[0m'

# ~enum
cat, less = range(2)


def init():
    help_message = '''wiki: access your information from anywhere
      create db/ folder and put your wiki text files with subjects as file names
      @Gal Pressman <galpressman@gmail.com>'''

    parser = argparse.ArgumentParser(description=help_message, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('subject', nargs='?', help='subject you are interested in')
    action_group = parser.add_mutually_exclusive_group()
    action_group.add_argument('-a', '--add', action='store_true', help='add a new subject to the wiki')
    action_group.add_argument('-e', '--edit', action='store_true', help='edit the subject\'s wiki')
    action_group.add_argument('-g', '--grep', action='store_true', help='grep your wiki')
    action_group.add_argument('-l', '--less', action='store_true', help='less instead of cat')
    args = parser.parse_args()

    # make sure arguments are ok
    check_args(parser, args)
    if not os.path.isdir(db_dir):
        print('db folder doesn\'t exist, exiting...')
        exit()

    return args


def check_args(parser, args):
    if (args.add or args.edit or args.grep) and not args.subject:
        parser.error('please provide a subject...')


def print_all_subjects():
    files = sorted(os.listdir(db_dir))

    print_subjects(files)


def print_subjects(subjects):
    for f in subjects:
        print('  # {}'.format(f))


def print_subject(file_name, opt):
    to_print = UNDERLINED_BLUE + file_name + ':' + VANILLA + '\n'

    with open(file_path.format(file=file_name), 'r') as f:
        for line in f:
            if line.startswith('#'):
                to_print += BLUE + line + VANILLA
            else:
                to_print += line
        if opt == less:
            pydoc.pipepager(to_print, 'less -R')
        else:
            print(to_print)


def extract_from_grep_line(line):
    # go over line until first occurrence of ':' or '-'
    pattern = re.compile('^\S*/(?P<subject>[^:-]*)[:-](?P<text>.*)$')
    match = pattern.match(line)

    if match:
        return match.group('subject'), match.group('text')
    return match, match  # None, None


def grep(subject):
    try:
        # we need to try because grep might not find anything and raise an exception
        output = subprocess.check_output(['grep', '-riC1', subject, db_dir])
        old_subject = ''

        for line in output.splitlines():
            subject, text = extract_from_grep_line(line)
            if not subject:
                continue

            if subject != old_subject:
                print('\n' + UNDERLINED_BLUE + subject + VANILLA)

            print(text)
            old_subject = subject
    except:
        pass


def vim(file_name):
    subprocess.call(['vim', file_path.format(file=file_name)])


def main():
    args = init()

    if not args.subject:
        # print all and exit
        print(UNDERLINED_BLUE + 'choose a subject:' + VANILLA)
        print_all_subjects()
        return 0

    if args.add:
        if os.path.isfile(file_path.format(file=args.subject)):
            print(BLUE + args.subject + ' already exists...' + VANILLA)
            return 0
        else:
            vim(args.subject)
            return 0

    if args.grep:
        grep(args.subject)
        return 0

    subject = args.subject
    files = sorted(os.listdir(db_dir))

    if os.path.isfile(file_path.format(file=args.subject)):
        # if file name is given explicitly bingo
        right_files = [args.subject]
    else:
        # else we need to "grep" our files
        right_files = [sfile for sfile in files if sfile.startswith(subject)]

    if len(right_files) == 1:
        # one matching file
        [file_name] = right_files
        if args.edit:
            vim(file_name)
        else:
            print_subject(file_name, less if args.less else cat)
    elif len(right_files) == 0:
        # no matching files
        print(UNDERLINED_BLUE + 'no matching files, available subjects:' + VANILLA)
        print_all_subjects()
    else:
        # too many matching files
        print(UNDERLINED_BLUE + 'choose between:' + VANILLA)
        print_subjects(right_files)


if __name__ == '__main__':
    main()
