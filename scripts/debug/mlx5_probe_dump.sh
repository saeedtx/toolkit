#!/bin/bash

#title           : mlx5_probe_dump.sh <ifname>
#description     : Using special perf probes this script will dump information of
#		   mlx5 netdev private structures and variables and HW resources numbers
#author          : Saeed Mahameed <saeedm@emllanox.com>
#version         : 0.1
USAGE="$0 <ifname> [perf porbe flags]"

function get_rqn {
	echo "rqn$1=priv->channels.c[$1]->rq.rqn:x32 rqcqn=priv->channels.c[$1]->rq.cq.mcq.cqn:x32"
}

function get_sqn {
	echo "sqn0_$1=priv->channels.c[$1]->sq[0].sqn:x32 sqcqn=priv->channels.c[$1]->sq[0].cq.mcq.cqn:x32"
}

function get_itirn {
	echo "rsstirn$1=priv->indir_tir[$1]->tirn:x32"
}

function get_itir_rqtn {
	echo "rqtn=priv->indir_tir[$i]->rqt.rqtn:x32"
}

function get_indir_rqt {
	 echo "rqt_rqi_$1=priv->channels.params.indirection_rqt[$1]:u32"
}

if [ -z "$1" ] ; then echo $USAGE; exit 1; fi
IFACE=$1
PFLAGS=$2

if [ -z "$(lsmod | grep mlx5_core)" ] ; then
	echo mlx5_core is not listed, Assuming kernel built-in module.
	MODULE_ARG=''
else
	MODULE_ARG='-m mlx5_core'
fi

PERFP_CMD="perf probe $PFLAGS $MODULE_ARG"

function probe_mlx5e_channels {
	IFACE=$1
	CHANNELS=$(ethtool -l $IFACE | grep -i combined | tail -1 | cut -d ":" -f2 | tr -d '[:space:]')
	echo $IFACE channels=$CHANNELS

	# add channels probes
	for i in $(seq 0 $(( $CHANNELS - 1 ))) ; do
		PROBE="mlx5e_ethtool_get_drvinfo $(get_rqn $i) $(get_sqn $i)"
		(set -x; $PERFP_CMD -f -a "$PROBE") #using subshell to enable -x only on this line
	done
}

function probe_mlx5e_rss_tirs {
	# Add RSS tirs rqts probes
	for i in $(seq 0 9) ; do
		PROBE="mlx5e_ethtool_get_drvinfo $(get_itirn $i) $(get_itir_rqtn $i)"
		(set -x; $PERFP_CMD -f -a "$PROBE")
	done
}

function probe_mlx5e_rss_rqt {
	# Add RSS rqt probes
	for i in $(seq 0 4 127) ; do
		PROBE="mlx5e_ethtool_get_drvinfo $(get_indir_rqt $i) $(get_indir_rqt $((i+1))) $(get_indir_rqt $((i+2))) $(get_indir_rqt $((i+3)))" 
		(set -x; $PERFP_CMD -f -a "$PROBE")
	done
}

function delete_probes {
	(set -x; perf probe -q -d 'mlx5e_ethtool_get_drvinfo*')
}

delete_probes

set -e

# Add probes
probe_mlx5e_channels $IFACE
probe_mlx5e_rss_tirs
probe_mlx5e_rss_rqt

set -x
# start recording the probe
perf record -e probe:mlx5e_ethtool_get_drvinfo* -aR sleep 1 &

sleep 0.25        # for record to take effect
ethtool -i $IFACE # stimulate the probe
wait              # wait for perf record
perf script | cat # dump perf report

# Delete the probes and exit
delete_probes
exit 0
