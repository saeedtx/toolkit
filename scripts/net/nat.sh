#!/bin/bash -x
#title           : nat.sh <wlan ifname> <bridge ifname> ["tap interfaces"]
#example:        : nat.sh wlp2s0 brnat0 "tap1 tap2"
#description     : Create a virtual bridge if not exists and configure a nat between it a physical interface
#                  and runs a Dns server over the bridge
#author          : saeedm@mellanox.com
#date            : Sat Sep 15 22:48:49 PDT 2018
#version         : 0.1
#note            : run as super user
#                : Tap interfaces are not must, you can ask the script to create them or you can create them
#                : manually later

wlanif=${1:-wlp2s0}
brif=${2:-brnat0}

killall dnsmasq
echo "cleanup iptables"
iptables -Fi
iptables -t nat -F
iptables -X
iptables -t nat -X

# Network information
NETWORK=192.168.0.0
NETMASK=255.255.0.0
GATEWAY=192.168.0.1
DHCPRANGE=192.168.0.10,192.168.0.254

ip link add name $brif type bridge
#ip addr add $GATEWAY/ dev $brif
#ip link set $brif up
ifconfig $brif "$GATEWAY" netmask "$NETMASK" up

firewall-cmd --reload

firewall-cmd --remove-interface=$brif

firewall-cmd --add-masquerade
firewall-cmd --zone=internal --add-interface $brif
firewall-cmd --zone=internal --add-service=dns --add-service=dhcp

#dnsmasq --interface=$brif --bind-interfaces  --dhcp-option-force=3,192.168.0.1 --dhcp-option-force=6,192.168.0.1 --dhcp-range=192.168.0.2,192.168.255.254
dnsmasq_options="--except-interface=lo --bind-interfaces --strict-order"
dnsmasq $dnsmasq_options --interface=$brif --dhcp-option-force=3,$GATEWAY --dhcp-option-force=6,$GATEWAY --dhcp-range=$DHCPRANGE

[[ ! -d /etc/qemu ]] && mkdir /etc/qemu
echo allow $brif >> /etc/qemu/bridge.conf
cat /etc/qemu/bridge.conf
sort /etc/qemu/bridge.conf | uniq > /tmp/bridge.conf
cp -rf /tmp/bridge.conf /etc/qemu/bridge.conf
cat /etc/qemu/bridge.conf

sysctl net.ipv4.ip_forward=1
sysctl net.ipv6.conf.default.forwarding=1
sysctl net.ipv6.conf.all.forwarding=1

iptables -t nat -A POSTROUTING -o $wlanif -j MASQUERADE
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i $brif -o $wlanif -j ACCEPT

if [ "$3" != "" ]
then
	for tapif in $3
	do
		ip tuntap add dev $tapif mode tap
		brctl addif $brif $tapif
		ifconfig $tapif up
	done
fi
